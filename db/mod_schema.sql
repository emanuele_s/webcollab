CREATE TABLE IF NOT EXISTS categories (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(200) NOT NULL,
            userid INT NOT NULL,
			created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			edited TIMESTAMP NOT NULL DEFAULT 0,
            FOREIGN KEY (userid) REFERENCES users(id)
)
ENGINE = InnoDB
CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS logging (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            user INT NOT NULL,
            type VARCHAR(50) NOT NULL,
            name VARCHAR(200) NOT NULL,
            entityid int NOT NULL,
            operation VARCHAR(50) NOT NULL,
            modified TIMESTAMP DEFAULT NOW()
)
ENGINE = InnoDB
CHARACTER SET = utf8;
            
CREATE TABLE IF NOT EXISTS deleted_tasks (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	parent INT UNSIGNED NOT NULL,
	name VARCHAR(255) NOT NULL,
	text TEXT,
	created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	edited TIMESTAMP NOT NULL DEFAULT 0,
	owner INT UNSIGNED NOT NULL DEFAULT 0,
	creator INT UNSIGNED NOT NULL,
	finished_time TIMESTAMP NOT NULL DEFAULT 0,
	projectid INT UNSIGNED NOT NULL DEFAULT 0,
	deadline TIMESTAMP NOT NULL DEFAULT 0,
	priority TINYINT NOT NULL DEFAULT 2,
	status VARCHAR(20) NOT NULL DEFAULT 'deleted',
	taskgroupid INT UNSIGNED NOT NULL,
	lastforumpost TIMESTAMP NOT NULL DEFAULT 0,
	usergroupid INT UNSIGNED NOT NULL,
	globalaccess VARCHAR(5) NOT NULL DEFAULT 't',
	groupaccess VARCHAR(5) NOT NULL DEFAULT 'f',
	lastfileupload TIMESTAMP NOT NULL DEFAULT 0,
	completed TINYINT NOT NULL DEFAULT 0,
	completion_time TIMESTAMP NOT NULL DEFAULT 0,
	archive TINYINT NOT NULL DEFAULT 0,
	sequence INT UNSIGNED NOT NULL DEFAULT 0,
	category INT,
	contact VARCHAR(200),
	INDEX (owner),
	INDEX (parent),
	INDEX (name(10)),
	INDEX (projectid),
	INDEX (taskgroupid),
	INDEX (deadline),
	INDEX (status, parent)
)
ENGINE = InnoDB
CHARACTER SET = utf8;

DELIMITER //

CREATE TRIGGER deleted_data
BEFORE DELETE
   ON tasks FOR EACH ROW
BEGIN
   INSERT INTO deleted_tasks
   ( 	id,
		parent,
		name,
		text,
		created,
		edited,
		owner,
		creator,
		finished_time,
		projectid,
		deadline,
		priority,
		status,
		taskgroupid,
		lastforumpost,
		usergroupid,
		globalaccess,
		groupaccess,
		lastfileupload,
		completed,
		completion_time,
		archive,
		sequence,
		category,
		contact,
		contmail )
   VALUES
   ( 	OLD.id,
		OLD.parent,
		OLD.name,
		OLD.text,
		OLD.created,
		OLD.edited,
		OLD.owner,
		OLD.creator,
		OLD.finished_time,
		OLD.projectid,
		OLD.deadline,
		OLD.priority,
		'deleted',
		OLD.taskgroupid,
		OLD.lastforumpost,
		OLD.usergroupid,
		OLD.globalaccess,
		OLD.groupaccess,
		OLD.lastfileupload,
		OLD.completed,
		OLD.completion_time,
		OLD.archive,
		OLD.sequence,
		OLD.category,
		OLD.contact,
		OLD.contmail );

END; //

DELIMITER ;

ALTER TABLE tasks 
ADD COLUMN category INT;

ALTER TABLE tasks 
ADD COLUMN contmail VARCHAR(200) DEFAULT NULL;

ALTER TABLE tasks
ADD COLUMN contact VARCHAR(200);

ALTER TABLE files
ADD COLUMN protocol VARCHAR(200);

ALTER TABLE files
ADD COLUMN status VARCHAR(200);

ALTER TABLE users 
ADD COLUMN morningtasks INT DEFAULT 0;

ALTER TABLE users
ADD COLUMN tech VARCHAR(5) NOT NULL DEFAULT 'f';

ALTER TABLE users
ADD COLUMN catmana VARCHAR(5) NOT NULL DEFAULT 'f';