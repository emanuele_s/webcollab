<?php
/*
  $Id: task_edit.php 2270 2009-08-14 06:58:03Z andrewsimpson $

  (c) 2002 - 2012 Andrew Simpson <andrew.simpson at paradise.net.nz>

  WebCollab
  ---------------------------------------
  Based on original file written for CoreAPM by Dennis Fleurbaaij, Andrew Simpson &
  Marshall Rose 2001/2002

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Edit a task

*/

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/details.php' );
include_once(BASE.'includes/time.php' );
include_once(BASE.'tasks/task_common.php' );

$content = '';
$javascript = '';
$allowed = array();
$new_owner = false;
$new_status = false;

//
//check user access
//
function user_access($owner, $usergroupid, $groupaccess, $v1 = 1, $v2 = 2) {

  global $GID, $TASKID_ROW;

  if(ADMIN ){
    return true;
  }
  if($v1 == $v2){
      return true;
  }
  if(GUEST ){
    return false;
  }
  if($owner == UID ){
    return true;
  }
  if($owner == 0 ) { //owner is nobody
    return true;
  }
  if($usergroupid == 0 ) {
    return false;
  }
  if($groupaccess == 't' ) {
    if(isset($GID[$usergroupid] ) ) {
      return true;
    }
  }  
  return false;
}

//
// List tasks for reparenting
//

function listReparentTasks($projectid, $self_parent ) {

  global $task_reparent, $task_projectid;
  global $task_array, $parent_array, $task_count, $level_count;

  //initialise variables
  $content = '';
  $task_array   = array();
  $parent_array = array();
  $task_count   = 0;  //counter for $task_array
  $level_count  = 1;  //number of task levels

  //search for tasks by projectid
  $task_key = array_keys((array)$task_projectid, $projectid );

  if(sizeof($task_key) < 1 ) {
    return;
  }

  //cycle through relevant tasks
  foreach((array)$task_key as $key ) {

    $task_array[$task_count]['id']     = $task_reparent[($key)]['id'];
    $task_array[$task_count]['parent'] = $task_reparent[($key)]['parent'];
    $task_array[$task_count]['name']   = $task_reparent[($key)]['name'];

    //if this is a subtask, store the parent id
    if($task_array[$task_count]['parent'] != $projectid ) {
      //store parent as array key for faster searching
      $parent_array[($task_array[$task_count]['parent'])] = 1;
    }
    ++$task_count;

    //remove used key to shorten future searches
    unset($task_projectid[$key] );
  }

  $padding = padding($level_count );

  //iteration for main tasks
  for($i=0 ; $i < $task_count ; ++$i ){

    //ignore subtasks in this iteration
    if(($task_array[$i]['parent'] != $projectid ) )  {
      continue;
    }

    //show line
    $content .= "<option value=\"".$task_array[$i]['id']."\"";

    if($self_parent == $task_array[$i]['id'] ) {
      $content .= " selected=\"selected\"";
    }
    $content .= ">".$padding.$task_array[$i]['name']."</option>\n";

    //if this task has children (subtasks), iterate recursively to find them
    if(isset($parent_array[($task_array[$i]['id'])] ) ) {
      $content .= find_children($task_array[$i]['id'], $self_parent );
    }
  }

  return $content;
}


//
//Check if the subtask is the last one to be completed
//
function check_child($task){
    $q = db_prepare('SELECT parent FROM '.PRE.'tasks WHERE id = '.$task);
    db_execute($q);
    
    $myid = db_fetch_array($q, 0);
    
    $parent = (int) $myid['parent'];
    
    $q1 = db_prepare('SELECT status FROM '.PRE.'tasks WHERE id != '.$task.' AND parent = '.(int) $myid['parent']);
    db_execute($q1);
    $status = db_fetch_all($q1);
    
    foreach ($status as $v) {
        if(strcmp($v['status'], 'done') != 0){
            return 1;
        }
    }
    return 0;
}

//
// List subtasks (recursive function)
//
function find_children($parent, $self_parent ) {

  global $task_array, $parent_array, $task_count, $level_count;

  ++$level_count;
  $content = '';

  $padding = padding($level_count );

  for($i=0 ; $i < $task_count ; ++$i ) {

    //ignore tasks not directly under this parent
    if($task_array[$i]['parent'] != $parent ){
      continue;
    }

    //show line
    $content .= "<option value=\"".$task_array[$i]['id']."\"";

    if($self_parent == $task_array[$i]['id'] ) {
      $content .= " selected=\"selected\"";
    }
    $content .= ">".$padding.$task_array[$i]['name']."</option>\n";

    //if this task has children (subtasks), iterate recursively to find them
    if(isset($parent_array[($task_array[$i]['id'])] ) ) {
      $content .= find_children($task_array[$i]['id'], $self_parent );
    }
  }
  --$level_count;

  return $content;
}

//
// Padding for reparenting list
//

function padding($level_count ) {

  $padding = '&nbsp;';
  $max_level = min($level_count, 4 );

  for($i=0; $i < $max_level; ++$i ) {
    $padding .= '&nbsp;&nbsp;';
  }

return $padding;
}

//  START MAIN PROGRAM

//get input data
if(! @safe_integer($_GET['taskid']) ){
  error('Task edit', 'The taskid input is not valid' );
}
$taskid = $_GET['taskid'];

if(isset($_GET['owner'] ) && @safe_integer($_GET['owner'] ) ) {
  $new_owner = $_GET['owner'];
}


//Qui si accorge che hai cliccato su 'I finish it!' in questo caso imposta status = 1
if(isset($_GET['status'] ) && $_GET['status'] ) {
  $new_status = 'done';
}

//generate_token
generate_token('tasks' );
  
$q1 = db_prepare('SELECT category, contact, contmail FROM '.PRE.'tasks WHERE id=?');
db_execute($q1, array($taskid));
$check = db_fetch_all($q1);
$q2 = db_prepare('SELECT userid FROM '.PRE.'categories WHERE id=?');
db_execute($q2, array($check[0]['category']));
$cat = db_fetch_all($q2);
$category = count($cat) == 0 ? -1 : $cat[0]['userid'];

//can this user edit this task ?
if( ! user_access($TASKID_ROW['owner'], $TASKID_ROW['usergroupid'], $TASKID_ROW['groupaccess'], UID, $category ) ) {
  warning($lang['access_denied'], $lang['no_edit'] );
}

//get list of common users in private usergroups that this user can view 
$q = db_query('SELECT '.PRE.'usergroups_users.usergroupid AS usergroupid,
                      '.PRE.'usergroups_users.userid AS userid 
                      FROM '.PRE.'usergroups_users 
                      LEFT JOIN '.PRE.'usergroups ON ('.PRE.'usergroups.id='.PRE.'usergroups_users.usergroupid)
                      WHERE '.PRE.'usergroups.private=1');

for( $i=0 ; $row = @db_fetch_num($q, $i ) ; ++$i ) {
  if(isset($GID[($row[0])] ) ) {
   $allowed[($row[1])] = $row[1];
  }
}

 $q = db_prepare('SELECT parent, projectid FROM '.PRE.'tasks WHERE id =?');
 db_execute($q, array($taskid));
 $myid = db_fetch_array($q, 0);

//start showing task info
if(isset($_GET['status'])){
     //is it a Supertask?
     if($myid['parent'] == $myid['projectid']){
        $content .= "<form method=\"post\" action=\"tasks.php\" onsubmit= \"return fieldCheck('name') && dateCheck() && checkContact() && confirm('".$lang['complete_task']."')\">\n".
                    "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
                    "<input type=\"hidden\" name=\"action\" value=\"submit_update\" />\n".
                    "<input type=\"hidden\" name=\"sub\" value=\"".$taskid."\" />".
                    "<input type=\"hidden\" id=\"token\" name=\"token\" value=\"".TOKEN."\" />\n".
                    "<input type=\"hidden\" name=\"taskid\" value=\"".$TASKID_ROW['id']."\" />\n".
                    "<input type=\"hidden\" id=\"alert_field\" name=\"alert1\" value=\"".$lang['missing_field_javascript']."\" />\n".
                    "<input type=\"hidden\" id=\"alert_field2\" name=\"alert4\" value=\"".$lang['fill_all_field']."\" />\n".
                    "<input type=\"hidden\" id=\"alert_date\" name=\"alert2\" value=\"".$lang['invalid_date_javascript']."\" />\n".
                    "<input type=\"hidden\" id=\"alert_finish\" name=\"alert3\" value=\"".$lang['finish_date_javascript']."\" />\n".
                    "<input type=\"hidden\" id=\"url\" name=\"url\" value=\"".$lang['url_javascript']."\" />\n".
                    "<input type=\"hidden\" id=\"image_url\" name=\"image_url\" value=\"".$lang['image_url_javascript']."\" />\n";
     }
     else{
         $content .= "<form method=\"post\" action=\"tasks.php\" onsubmit= \"return fieldCheck('name') && dateCheck() && checkContact() && confirm('".$lang['confirm']."')\">\n".
            "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
            "<input type=\"hidden\" name=\"action\" value=\"submit_update\" />\n".
            "<input type=\"hidden\" name=\"complete\" value=\"1\" />".
            "<input type=\"hidden\" id=\"token\" name=\"token\" value=\"".TOKEN."\" />\n".
            "<input type=\"hidden\" name=\"taskid\" value=\"".$TASKID_ROW['id']."\" />\n".
            "<input type=\"hidden\" id=\"alert_field\" name=\"alert1\" value=\"".$lang['missing_field_javascript']."\" />\n".
            "<input type=\"hidden\" id=\"alert_date\" name=\"alert2\" value=\"".$lang['invalid_date_javascript']."\" />\n".
            "<input type=\"hidden\" id=\"alert_finish\" name=\"alert3\" value=\"".$lang['finish_date_javascript']."\" />\n".
            "<input type=\"hidden\" id=\"alert_field2\" name=\"alert4\" value=\"".$lang['fill_all_field']."\" />\n".
            "<input type=\"hidden\" id=\"url\" name=\"url\" value=\"".$lang['url_javascript']."\" />\n".
            "<input type=\"hidden\" id=\"image_url\" name=\"image_url\" value=\"".$lang['image_url_javascript']."\" />\n";
     }
}
else{
    $content .= "<form method=\"post\" action=\"tasks.php\" onsubmit= \"return fieldCheck('name') && dateCheck() && checkContact() && confirm('".$lang['confirm']."')\">\n".
                "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
                "<input type=\"hidden\" name=\"action\" value=\"submit_update\" />\n".
                "<input type=\"hidden\" id=\"token\" name=\"token\" value=\"".TOKEN."\" />\n".
                "<input type=\"hidden\" name=\"taskid\" value=\"".$TASKID_ROW['id']."\" />\n".
                "<input type=\"hidden\" id=\"alert_field\" name=\"alert1\" value=\"".$lang['missing_field_javascript']."\" />\n".
                "<input type=\"hidden\" id=\"alert_date\" name=\"alert2\" value=\"".$lang['invalid_date_javascript']."\" />\n".
                "<input type=\"hidden\" id=\"alert_finish\" name=\"alert3\" value=\"".$lang['finish_date_javascript']."\" />\n".
                "<input type=\"hidden\" id=\"alert_field2\" name=\"alert4\" value=\"".$lang['fill_all_field']."\" />\n".
                "<input type=\"hidden\" id=\"url\" name=\"url\" value=\"".$lang['url_javascript']."\" />\n".
                "<input type=\"hidden\" id=\"image_url\" name=\"image_url\" value=\"".$lang['image_url_javascript']."\" />\n";
}

//select either project or task for text
switch($TYPE) {
  case 'project':
    $content .= "<input id=\"projectDate\" type=\"hidden\" name=\"projectDate\" value=\"-1\" />\n".
                //no taskgroups in projects
                "<input type=\"hidden\" name=\"taskgroupid\" value=\"0\" /></fieldset>\n ".
                "<table class=\"celldata\">\n".
                "<tr><td>".$lang['creation_time']."</td><td>".nicedate($TASKID_ROW['created'] )."</td></tr>\n".
                "<tr><td>".$lang['project_name'].":</td><td><input id=\"name\" type=\"text\" name=\"name\" class=\"size\" value=\"".$TASKID_ROW['name']."\" /></td></tr>\n";
    break;

  case 'task':
    //get parent details
    $q = db_prepare('SELECT '.db_epoch().'deadline) AS epoch_deadline, status FROM '.PRE.'tasks WHERE id=? LIMIT 1' );
    db_execute($q, array($TASKID_ROW['parent'] ) );
    $parent_row = db_fetch_array($q, 0 );

    switch ($parent_row['status'] ) {
      case 'created':
      case 'active':
        //add the project deadline (plus GMT offset) for the javascript
        $project_deadline = $parent_row['epoch_deadline'] + TZ*60*60;
        break;

      case 'notactive':
      case 'cantcomplete':
      case 'done':
      case 'nolimit':
      default:
        //don't check project deadline with inactive parents
        $project_deadline = -1;
        break;
    }

    //get project name
    $q = db_prepare('SELECT name FROM '.PRE.'tasks WHERE id=? LIMIT 1' );
    db_execute($q, array($TASKID_ROW['projectid'] ) );
    $project_name = db_result($q, 0, 0 );

    //show project finish date for javascript & other details
    $content .=  "<input id=\"projectDate\" type=\"hidden\" name=\"projectDate\" value=\"".$project_deadline."\" /></fieldset>\n".
                 "<table class=\"celldata\">\n".
                 "<tr><td>".$lang['creation_time']."</td><td>".nicedate($TASKID_ROW['created'] )."</td></tr>\n".
                 "<tr><td>".$lang['project'] .":</td><td><a href=\"tasks.php?x=".X."&amp;action=show&amp;taskid=".$TASKID_ROW['projectid']."\">".$project_name."</a></td></tr>\n";
    break;
}

//reparenting
$content .= "<tr><td>".$lang['parent_task'].":</td><td><select name=\"parentid\">\n";
$content .= "<option value=\"0\"";

if($TASKID_ROW['parent'] == 0 ){
  $content .= " selected=\"selected\"";
}
$content .= ">".$lang['no_reparent']."</option>\n";

//get tasks for reparenting and store for later use
if($TASKID_ROW['parent'] == 0 ) {
  //For project: Don't show tasks under
  $q = db_prepare('SELECT id, name, parent, projectid FROM '.PRE.'tasks
                        WHERE id<>? AND parent<>0 AND projectid<>?'.usergroup_tail().'AND archive=0 ORDER BY name');

  db_execute($q, array($taskid, $taskid ) );
}
else {
  //For task: Don't show child tasks under
  $q = db_prepare('SELECT id, name, parent, projectid FROM '.PRE.'tasks
                        WHERE id<>? AND (parent<>0 OR parent<>?)'.usergroup_tail().' AND archive=0 ORDER BY name');

  db_execute($q, array($taskid, $taskid ) );
}

for( $i=0 ; $reparent_row = @db_fetch_array($q, $i ) ; ++$i ) {
  //put values into array
  $task_reparent[$i]['id']     = $reparent_row['id'];
  $task_reparent[$i]['name']   = $reparent_row['name'];
  $task_reparent[$i]['parent'] = $reparent_row['parent'];
  $task_projectid[$i]          = $reparent_row['projectid'];
}

//get projects for reparenting
$q = db_prepare('SELECT id, name FROM '.PRE.'tasks WHERE parent=0 AND id<>?'.usergroup_tail().' AND archive=0 ORDER BY name');
db_execute($q, array($taskid ) );

for( $i=0 ; $reparent_row = @db_fetch_array($q, $i ) ; ++$i ) {

  $content .= "<option value=\"".$reparent_row['id']."\"";

  if($TASKID_ROW['parent'] == $reparent_row['id'] ) {
    $content .= " selected=\"selected\"";
  }
  $content .= ">+&nbsp;".$reparent_row['name']."</option>\n";

  //add tasks previously stored
  $content .= listReparentTasks($reparent_row['id'], $TASKID_ROW['parent'] );
}

$content .="</select></td></tr>\n";

//show task (if applicable)
if($TASKID_ROW['parent'] != 0 ){
  $content .= "<tr><td>".$lang['task_name'].":</td><td><input id=\"name\" type=\"text\" name=\"name\" class=\"size\" value=\"".$TASKID_ROW['name']."\" /></td></tr>\n";
}
//deadline
$content .= "<tr><td>".$lang['deadline'].":</td><td>".date_select_from_timestamp($TASKID_ROW['deadline'])."</td></tr>\n";

//priority
$s1 = ""; $s2 = ""; $s3 = ""; $s4 =""; $s5 = "";

switch($TASKID_ROW['priority'] ) {
  case 0:
    $s1 = "selected=\"selected\"";
    break;

  case 1:
    $s2 = " selected=\"selected\"";
    break;

  case 3:
    $s4 =" selected=\"selected\"";
    break;

  case 4:
    $s5 = " selected=\"selected\"";
    break;

  case 2:
  default:
    $s3 = " selected=\"selected\"";
    break;
}

$content .= "<tr><td>".$lang['priority'].":</td><td>\n".
            "<select name=\"priority\">\n".
            "<option value=\"0\"".$s1.">".$task_state['dontdo']."</option>\n".
            "<option value=\"1\"".$s2.">".$task_state['low']."</option>\n".
            "<option value=\"2\"".$s3.">".$task_state['normal']."</option>\n".
            "<option value=\"3\"".$s4.">".$task_state['high']."</option>\n".
            "<option value=\"4\"".$s5.">".$task_state['yesterday']."</option>\n".
            "</select></td></tr>\n";

$s1 = ""; $s2 = ""; $s3 = ""; $s4 = "";

switch($TASKID_ROW['parent'] ){
  case 0:
    //status for projects - 'done' is calculated from tasks
    switch($TASKID_ROW['status'] ) {
      case 'notactive':
        $s1 = " selected=\"selected\"";
        break;

      case 'nolimit':
        $s2 = " selected=\"selected\"";
        break;

      case 'cantcomplete':
        $s4 =" selected=\"selected\"";
        break;

      case 'active':
      default:
        $s3 = " selected=\"selected\"";
        break;
    }
    $content .= "<tr><td>".$lang['status'].":</td><td>\n".
                 "<select name=\"status\">\n".
                 "<option value=\"notactive\"".$s1.">".$task_state['planned_project']."</option>\n".
                 "<option value=\"nolimit\"".$s2.">".$task_state['no_deadline_project']."</option>\n".
                 "<option value=\"active\"".$s3.">".$task_state['active_project']."</option>\n".
                 "<option value=\"cantcomplete\"".$s4.">".$task_state['cantcomplete']."</option>\n".
                 "</select></td></tr>";
    break;

    default:
      //status for tasks
      if($new_status !== false ) {
        $selection = $new_status;
      }
      else {
        $selection = $TASKID_ROW['status'];
      }

     $s1 = ""; $s2 = ""; $s3 = ""; $s4 =""; $s5 = "";

      switch($selection ) {
        case 'notactive':
          $s2 = " selected=\"selected\"";
          break;

        case 'active':
          $s3 = " selected=\"selected\"";
          break;

        case 'cantcomplete':
          $s4 =" selected=\"selected\"";
          break;

        case 'done':
          $s5 = " selected=\"selected\"";
          break;

        case 'created':
        default:
          $s1 = " selected=\"selected\"";
          break;
      }
      $content .= "<tr><td>".$lang['status'].":</td><td>\n".
                   "<select id=\"projectStatus\" name=\"status\">\n".
                   "<option value=\"created\"".$s1.">".$task_state['new']."</option>\n".
                   "<option value=\"notactive\"".$s2.">".$task_state['planned']."</option>\n".
                   "<option value=\"active\"".$s3.">".$task_state['active']."</option>\n".
                   "<option value=\"cantcomplete\"".$s4.">".$task_state['cantcomplete']."</option>\n".
                   "<option value=\"done\"".$s5.">".$task_state['completed']."</option>\n".
                   "</select></td></tr>";
}

//check if new task owner has been preset
if($new_owner !== false ) {
  $selection = $new_owner;
}
else {
  $selection = $TASKID_ROW['owner'];
}

//Sys admin or category admin
if(ADMIN || UID == $category){
    $content .= "<tr class=\"ui-widget\"><td><label for=\"tags\">".$lang['contact_person']."</label></td><td><input id=\"tags\" type=\"text\" name=\"contact\" value=\"".$check[0]['contact']."\"></td></tr>\n";
    $content .= "<tr><td><label for=\"contmail\">".$lang['contactmail'].":</label></td><td><input id=\"contmail\" type=\"text\" name=\"contmail\" value=\"".$check[0]['contmail']."\"></td></tr>\n";
    $content .= "<tr><td>".$lang[$TYPE."_owner"].":</td><td><select name=\"owner\">\n";
}
else{
    $content .= "<tr><td>".$lang[$TYPE."_owner"].":</td><td><select name=\"owner\" disabled>\n";
}
if($selection == 0 ) {
  $content .= "<option value=\"0\" selected=\"selected\">".$lang['nobody']."</option>\n";
}
else {
  $content .= "<option value=\"0\">".$lang['nobody']."</option>\n";
}

//get current user list
$q = db_query('SELECT id, fullname, private FROM '.PRE.'users WHERE deleted=\'f\' AND guest=0 AND tech=\'f\' ORDER BY fullname' );

for( $i=0 ; $user_row = @db_fetch_array($q, $i ) ; ++$i ) {

  //user test for privacy
  if($user_row['private'] && ($user_row['id'] != UID ) && ( ! ADMIN ) && ( ! isset($allowed[($user_row['id'])] ) ) ){
    continue;
  }

  $content .= "<option value=\"".$user_row['id']."\"";

  if($selection == $user_row['id'] ){
    $content .= " selected=\"selected\"";
    $utentetemp=$user_row['id'];
  }
  $content .= ">".$user_row['fullname']."</option>\n";
}

$content .= "</select></td></tr>\n";

if(ADMIN){
    $content .= "<tr><td>".$lang['choose_category']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"textlink\">[<a href=\"".BASE_URL."categories.php?x=".X."&action=add&taskid=".$taskid."&redirect=edit\">New Category</a>]</td><td><select name=\"category\">".
                "<option value=\"-1\" >".$lang['select_cat']."</option>\n";
    $q = db_query('SELECT id, name FROM '.BASE.'categories');
    $categories = db_fetch_all($q);
    
    foreach ($categories as $cat){
        if($cat['id'] == $check[0]['category']){
            $content .= "<option value=\"".$cat['id']."\" selected=\"selected\">".$cat['name']."</option>\n";
        }else{
            $content .= "<option value=\"".$cat['id']." \">".$cat['name']."</option>\n";
        }
    }
}

if (!ADMIN){
    $content .= "<input type=\"hidden\" id=\"owner\" name=\"owner\" value=\"".$utentetemp."\" />";
}
//show a selection box with the taskgroups
//  (projects don't have taskgroups)
if($TASKID_ROW['parent'] != 0 ) {

  $content .= "<tr><td><a href=\"help/help_language.php?item=taskgroup&amp;type=help&amp;lang=".LOCALE_USER."\" onclick=\"window.open('help/help_language.php?item=taskgroup&amp;type=help&amp;lang=".LOCALE_USER."'); return false\">".$lang['taskgroup']."</a>: </td><td><select name=\"taskgroupid\">\n";
  $content .= "<option value=\"0\">".$lang['no_group']."</option>\n";

  $q = db_query('SELECT id, name FROM '.PRE.'taskgroups ORDER BY name' );

  for( $i=0 ; $taskgroup_row = @db_fetch_array($q, $i ) ; ++$i) {

    $content .= "<option value=\"".$taskgroup_row['id']."\"";

    if($TASKID_ROW['taskgroupid'] == $taskgroup_row['id'] ) {
      $content .= " selected=\"selected\"";
    }
    $content .= ">".$taskgroup_row['name']."</option>\n";
  }
  $content .= "</select></td></tr>\n";
}

//show all user-groups
$content .= "<tr><td><a href=\"help/help_language.php?item=usergroup&amp;type=help&amp;lang=".LOCALE_USER."\" onclick=\"window.open('help/help_language.php?item=usergroup&amp;type=help&amp;lang=".LOCALE_USER."'); return false\">".$lang['usergroup']."</a>: </td><td><select name=\"usergroupid\">\n";
$content .= "<option value=\"0\">".$lang['no_group']."</option>\n";

$q = db_query('SELECT id, name, private FROM '.PRE.'usergroups ORDER BY name' );

for( $i=0 ; $usergroup_row = @db_fetch_array($q, $i ) ; ++$i ) {

  //usergroup test for privacy
  if( (! ADMIN ) && ($usergroup_row['private'] ) && ( ! isset($GID[($usergroup_row['id'])] ) ) ) {
    continue;
  }

  $content .= "<option value=\"".$usergroup_row['id']."\"";

    if( $TASKID_ROW['usergroupid'] == $usergroup_row['id'] ) {
      $content .= " selected=\"selected\" >\n";
    }
    else {
      $content .= ">\n";
    }
    $content .= $usergroup_row['name']."</option>\n";
}
$content .= "</select></td></tr>\n";

//check box defaults
$global = ($TASKID_ROW['globalaccess'] == 't' ) ? "checked=\"checked\"" : '';
$group  = ($TASKID_ROW['groupaccess']  == 't' ) ? "checked=\"checked\"" : '';

$content .= "<tr><td><a href=\"help/help_language.php?item=globalaccess&amp;type=help&amp;lang=".LOCALE_USER."\" onclick=\"window.open('help/help_language.php?item=globalaccess&amp;type=help&amp;lang=".LOCALE_USER."'); return false\">".$lang['all_users_view']."</a></td><td><input type=\"checkbox\" name=\"globalaccess\" ".$global." /></td></tr>\n".
             "<tr><td><a href=\"help/help_language.php?item=groupaccess&amp;type=help&amp;lang=".LOCALE_USER."\" onclick=\"window.open('help/help_language.php?item=groupaccess&amp;type=help&amp;lang=".LOCALE_USER."'); return false\">".$lang['group_edit']."</a></td><td><input type=\"checkbox\" name=\"groupaccess\" ".$group." /></td></tr>\n".

             "<tr><td>".$lang[$TYPE."_description"]."</td>".
             "<td><script type=\"text/javascript\"> edToolbar('text');</script>".
             "<textarea name=\"text\" id=\"text\" rows=\"10\" cols=\"60\">".$TASKID_ROW['text']."</textarea></td></tr>\n".

             //do we need to email ?
             "<tr><td><label for=\"mailowner\">".$lang['email_new_owner']."</label></td><td><input type=\"checkbox\" name=\"mailowner\" id=\"mailowner\" ".DEFAULT_OWNER." /></td></tr>\n".
             "<tr><td><label for=\"maillist\">".$lang['email_group']."</label></td><td><input type=\"checkbox\" name=\"maillist\" id=\"maillist\" ".DEFAULT_GROUP." /></td></tr>\n";
            

             //Is it the last subtask ?
             if(check_child($taskid) == 0 && isset($_GET['status'] ) && $_GET['status']){
                 if($myid['parent'] == $myid['projectid']){
                    $content .=  "<tr><td><label for=\"completeSuper\">".$lang['complete_super']."</label></td><td><input type=\"checkbox\" name=\"completeSuper\" id=\"completeSuper\" value=\"".(int) $myid['parent']."\" disabled/></td></tr>\n".
                    "<input type=\"hidden\" name=\"sub\" value=\"".$TASKID_ROW['id']."\" />\n".
                    "</table>\n".
                    "<p><input type=\"submit\" value=\"".$lang['submit_changes']."\" /></p>".
                    "</form>\n";
                 }
                 else{
                     $content .=  "<tr><td><label for=\"completeSuper\">".$lang['complete_super']."</label></td><td><input type=\"checkbox\" name=\"completeSuper\" id=\"completeSuper\" value=\"".(int) $myid['parent']."\" checked=\"checked\" /></td></tr>\n".
                     "</table>\n".
                     "<p><input type=\"submit\" value=\"".$lang['submit_changes']."\" /></p>".
                     "</form>\n";
                 }
             }
             else{
                 $q = db_prepare('SELECT parent FROM '.PRE.'tasks WHERE id = '.$taskid);
                 db_execute($q);
                 $myid = db_fetch_array($q, 0);
                 $content .=  "<tr><td><label for=\"completeSuper\">".$lang['complete_super']."</label></td><td><input type=\"checkbox\" name=\"completeSuper\" id=\"completeSuper\" value=\"".(int) $myid['parent']."\" disabled/></td></tr>\n".
                 "</table>\n".
                 "<p><input type=\"submit\" value=\"".$lang['submit_changes']."\" /></p>".
                 "</form>\n";
             }
             

//delete options
if(ADMIN || UID == $category) { 

  $content .= "<form method=\"post\" action=\"tasks.php\" ".
              "onsubmit=\"return confirm('".sprintf($lang["del_javascript_".$TYPE."_sprt"], javascript_escape($TASKID_ROW['name'] ) )."')\">\n".
              "<fieldset><input type=\"hidden\" name=\"x\" value=\"".X."\" />".
              "<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
              "<input type=\"hidden\" name=\"taskid\" value=\"".$TASKID_ROW['id']."\" />\n".
              "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" /></fieldset>\n".
              "<p><input type=\"submit\" value=\"".$lang["delete_".$TYPE]."\" /></p>\n".
              "</form>\n";
}

new_box($lang["edit_".$TYPE], $content );

?>