<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Send daily mail to users
 */

require_once('path.php' );
require_once(BASE.'path_config.php' );
require_once(BASE_CONFIG.'config.php' );
include_once(BASE.'database/database.php');

function enable_login($userid, $username, $ip='0.0.0.0') {
  //create session key
  //use Mersenne Twister algorithm (random number), then one-way hash to give session key
  $session_key = sha1(mt_rand().mt_rand().mt_rand() );

  //remove the old login information
  $q = db_prepare('DELETE FROM '.PRE.'logins WHERE user_id=?' );
  @db_execute($q, array($userid ) );
  $q = db_prepare('DELETE FROM '.PRE.'login_attempt WHERE last_attempt < (now()-INTERVAL '.db_delim('20 MINUTE' ).') OR name=?' );
  @db_execute($q, array($username ) );
  @db_query('DELETE FROM '.PRE.'tokens WHERE lastaccess < (now()-INTERVAL '.db_delim(TOKEN_TIMEOUT.' MINUTE' ).')' );

  //log the user in
  $q = db_prepare('INSERT INTO '.PRE.'logins(user_id, session_key, ip, lastaccess ) VALUES (?, ?, ?, now() )' );
  @db_execute($q, array($userid, $session_key, $ip ) );

  //try and set a session cookie (if the browser will let us)
  $url = parse_url(BASE_URL );
  //use HTTP only to reduce XSS attacks (only in PHP 5.2.0+ )
  setcookie('webcollab_session', $session_key, 0, $url['path'], $url['host'], false, true );
  //(No need to record an error here if unsuccessful: the code will revert to URI session keys)

  //relocate the user to the main screen
  //(we use both URI session key and cookies initially - in case cookies don't work)
  if(isset($_GET['morning'])){
      header('Location: '.BASE_URL.'daily_mail.php?x='.$session_key.'&morning=1');
  }else{
        header('Location: '.BASE_URL.'daily_mail.php?x='.$session_key );
  }
  die;
}


$ip = $_SERVER['REMOTE_ADDR'];

enable_login(1, 'admin', $ip);

?>
