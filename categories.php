<?php

/*
  $Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  All functions and code needed to manage tasks' categories

*/

require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/screen.php' );
include_once(BASE.'includes/time.php' );

//Take the action
if(isset($_POST['action'])){
    $action = $_POST['action'];
}
elseif(isset($_GET['action'])){
    $action = $_GET['action'];
}
else{
    error('Categories action handler', 'No action given!');
}

switch($action){
    case 'add':
        create_top($lang['add_category'], 0, 'category-add', 2 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_add.php' );
        create_bottom();
        break;
    case 'edit':
        create_top($lang['edit_category'], 0, 'category-edit', 2 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_edit.php' );
        create_bottom();
        break;
    case 'remove':
        create_top($lang['remove_category'], 0, 'category-delete', 2 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_delete.php' );
        create_bottom();
        break;
    case 'show':
        create_top($lang['show_category'], 0, 'project-list' );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_show.php');
        create_bottom();
        break;
    case 'estimate':
        create_top($lang['show_estimate'], 0, 'project-list',2 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_estimate.php');
        create_bottom();
        break;
    //New category submit
    case 'submit_insert':
        include(BASE.'categories/category_sub_add.php');
        break;
    case 'submit_edit':
        include(BASE.'categories/category_sub_edit.php');
        break;
    case 'submit_delete':
        include(BASE.'categories/category_sub_delete.php');
        break;
    case 'submit_estimate':
        create_top($lang['show_estimate'], 0, 'project-list',2 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_sub_estimate.php');
        create_bottom();
        break;
    //Error case
    case 'visualize_estimate':
        create_top($lang['show_estimate'], 0, 'project-list',3 );
        include(BASE.'includes/mainmenu.php' );
        include(BASE.'categories/category_menubox.php');
        goto_main();
        include(BASE.'categories/category_visualize_estimate.php');
        create_bottom();
        break;        
    default:
         error('Categories action handler', 'Invalid request');
         break;
}
?>
