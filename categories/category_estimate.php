<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Show estimates menu
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'users/user_common.php' );

//admins only
if(! ADMIN ){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('category_estimate' );

$count = 0;

$content = "<form method=\"post\" action=\"categories.php?x=".X."&action=submit_estimate\">\n".  
           "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
           "<table class=\"celldata\">\n";
if(isset($_GET['error'])){ 
    $content .= "<tr><td><p style=\"color:red;\" >".$lang[$_GET['error']]."</p></td></tr>";
}
$content .= "<tr><td><input type=\"checkbox\" onClick=\"selectall(this)\" >".$lang['select_all']."</td></tr>\n".
            "<tr>";

$q = db_query('SELECT id, name FROM '.PRE.'categories ORDER BY name');
$categories = db_fetch_all($q);
   
foreach ($categories as $cate){
    if($count < 4){
        $count += 1;
        $content .= "<td><input type=\"checkbox\" name=\"cate[]\" value=\"".$cate['id']."\">".$cate['name']."</td>\n";
    }else{
        $content .= "</tr><tr><td><input type=\"checkbox\" name=\"cate[]\" value=\"".$cate['id']."\">".$cate['name']."</td>\n";
        $count = 0;
    }   
}
$content .= "</tr></table>".
            "<p><input type=\"submit\" value=\"".$lang['calc']."\" /></p>\n".
            "</form>";

new_box($lang['show_estimate'], $content );


?>
