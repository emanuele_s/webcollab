<?php

/*
  $Id$

  (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  This shows the menubox for the taskscategories-part.
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//secure variables
$content   = '';
$taskid    = -1;

//guests shouldn't get here
if(GUEST ) {
  return;
}

$content .= "<ul class=\"menu\">\n";

$content .= "<li><a href=\"categories.php?x=".X."&amp;action=add\">".$lang['add_category']."</a></li>\n".
            "<li><a href=\"categories.php?x=".X."&amp;action=edit\">".$lang['edit_category']."</a></li>\n".
            "<li><a href=\"categories.php?x=".X."&amp;action=remove\">".$lang['remove_category']."</a></li>\n".
            "<li><a href=\"categories.php?x=".X."&amp;action=show\">".$lang['show_category']."</a></li>\n";
if(ADMIN){
$content .= "<li><a href=\"categories.php?x=".X."&amp;action=estimate\">".$lang['show_estimate']."</a></li>\n";
}
$content .= "</ul>\n";

//show the box
new_box($lang['categories'], $content, 'boxdata-menu', 'head-menu', 'boxstyle-menu' );

?>
