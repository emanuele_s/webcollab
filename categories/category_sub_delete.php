<?php

/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Delete categories
 */

include_once(BASE.'users/user_common.php' );
require_once(BASE.'includes/token.php' );

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/time.php' );

//deny guest users
if(GUEST ) {
 warning($lang['access_denied'], $lang['not_owner'] );
}

$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

validate_token($token, 'category_delete');

if(!isset($_POST['category'])){
    warning('Delete Error', 'Error in Deleting Category');
}
$category = $_POST['category'];

$q = db_prepare('SELECT name FROM '.PRE.'tasks WHERE category=? AND status!=\'done\'');
db_execute($q, array($category));
$test = db_fetch_all($q);

if(count($test) != 0){
    header("Location: ".BASE_URL."categories.php?x=".X."&action=remove&error=e2&category=".$category);
    die;
}

$q = db_prepare('SELECT id, name FROM '.PRE.'tasks WHERE category=?');
db_execute($q, array($category));
$test = db_fetch_all($q);

$q1 = db_prepare('SELECT userid, name FROM '.PRE.'categories WHERE id=?');
db_execute($q1, array($category));
$oldcat = db_fetch_all($q1);

$q2 = db_prepare('SELECT count(id) AS num FROM '.PRE.'categories WHERE userid=? AND id!=?');
db_execute($q2, array($oldcat[0]['userid'], $category));
$numcat = db_fetch_all($q2);  

db_begin();

if($numcat[0]['num'] == 0){
    $q5 = db_prepare('UPDATE '.PRE.'users SET catmana=? WHERE id=?');
    db_execute($q5, array('f', $oldcat[0]['userid']));                
}

if(count($test) != 0){
    foreach ($test as $t){
        $q5 = db_prepare('UPDATE '.PRE.'tasks SET category=null WHERE id=?');
        db_execute($q5, array($t['id']));      
        $q8 = db_prepare('INSERT INTO '.BASE.'logging (user, type, name, entityid, operation) VALUES (?, ?, ?, ?, ?)');
        db_execute($q8, array(UID, 'task', $t['name'], $t['id'], 'edit_delete_category' ));
    }
}

$q7 = db_prepare('DELETE FROM '.PRE.'categories WHERE id=?');
db_execute($q7, array($category));

$q8 = db_prepare('INSERT INTO '.BASE.'logging (user, type, name, entityid, operation) VALUES (?, ?, ?, ?, ?)');
db_execute($q8, array(UID, 'category', $oldcat[0]['name'], $category, 'delete' ));

db_commit();

header("Location: ".BASE_URL."main.php?x=".X);
die;
?>
