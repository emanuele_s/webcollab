<?php

/*
  $Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Show all categories
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'users/user_common.php' );

//admins only
if(! ADMIN && !TECH){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('category_show' );


$content = '<div align="center"><br><table style="border-collapse:collapse;"><tr><th style="border: 1px solid #000; padding:5px;">'.$lang['categoryTable1'].'</th><th style="border: 1px solid #000; padding: 5px;">'.$lang['categoryTable2'].'</th></tr>';

//start of page


$q = db_query('SELECT name, userid FROM '.PRE.'categories ORDER BY name ASC;');
$cat = db_fetch_all($q);



foreach ($cat as $c){
    $q = db_prepare('SELECT fullname FROM '.PRE.'users WHERE id=?');
    db_execute($q, array($c['userid']));
    $user = db_fetch_array($q, 0);
    $content .= '<tr><td style="border: 1px solid #000; padding: 5px;">'.$c['name'].'</td><td style="border: 1px solid #000; padding: 5px;">'.$user['fullname'].'</td></tr>';
    //$content .= sprintf($lang['show_cat1'], $c['name'])."<br>".sprintf($lang['show_cat2'], $user['fullname'])."<br><br>";
}

$content .= "</table></div>";

new_box($lang['category_info'], $content);

?>
