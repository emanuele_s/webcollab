<?php

/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Delete categories
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'users/user_common.php' );

//admins only
if(! ADMIN && !TECH){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('category_delete' );

$content =  "<form method=\"post\" action=\"categories.php\" ".
        "onsubmit=\"return confirm('".$lang['confirm']."') \">\n".
        "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_delete\" />\n".
        "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
        "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
        "<table class=\"celldata\">\n";

if(isset($_GET['error'])){
    if(!TECH){
        $q = db_prepare('SELECT name FROM '.BASE.'tasks WHERE category=? AND status!=\'done\'');
        db_execute($q, array((int) $_GET['category']));
        $tasks = db_fetch_all($q);
        $content .= "<tr><td><p style=\"color:red;\" >".$lang['e2']."</p></td></tr>".
                    "<tr><td><p style=\"color:red;\">".$lang['change_task']."</p></td>".
                    "<td><ul style=\"list-style-type=circle;\">";

        foreach ($tasks as $t){
            $content .= "<li><p style=\"color:red;\">".sprintf($lang['free_string'], $t['name'])."</p></li>";
        }
         $content .= "</td></tr>";
    }else{
        $content .= "<tr><td><p style=\"color:red;\" >".$lang['e2']."</p></td></tr>";
    }
}

$content .= "<tr><td>".$lang['cat_del']."<select id=\"category\" name=\"category\">";
$q = db_query('SELECT id, name FROM '.BASE.'categories ORDER BY name');
$categories = db_fetch_all($q);

foreach ($categories as $cat){
    $content .= "<option value=\"".$cat['id']."\">".$cat['name']."</option>\n";
}

$content .= "</tr></table>".
            "<p><input type=\"submit\" value=\"".$lang['remove']."\" /></p>\n".
            "</form>";

new_box($lang['category_info'], $content );

?>
