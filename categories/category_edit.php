<?php
/*
  $Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Edit a category
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'users/user_common.php' );

//admins only
if(! ADMIN && !TECH){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('category_edit' );

$content =  "<form method=\"post\" action=\"categories.php\" ".
        "onsubmit=\"return fieldCheck('newname') \">\n".
        "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_edit\" />\n".
        "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
        "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
        "<input type=\"hidden\" id=\"alert_field\" name=\"alert2\" value=\"".$lang['no_changes']."\" /></fieldset>\n".
        "<table class=\"celldata\">\n";

if(isset($_GET['error'])){
    $content .= "<tr><td><p style=\"color:red;\" >".$lang['e1']."</p></td></tr>";
}

$content .= "<tr><td>".$lang['category']."</td><td><select name=\"category\" id=\"changingcombo\" onchange=\"changeText('changingcombo', 'newname')\">";

//Select one user to be the adminnistrator of the new category
$q = db_query('SELECT id, name FROM '.PRE.'categories ORDER BY name');
$categories = db_fetch_all($q);

if(empty($categories)){
    warning('No category', 'There are no category to edit in the system you will be redirected in 5 seconds in order to create a new category.',1, 5, "categories.php?x=".X."&action=add");
}

foreach($categories as $category) {
   $content .= "<option value=\"".$category['id']."\" ";
   $content .= ">".$category['name']."</option>\n";
}
            
$content .= "</tr>".
            "<tr><td>".$lang['newname']."</td><td><input id=\"newname\" type=\"text\" name=\"newname\" class=\"size\" value=\"".$categories[0]['name']."\" />".
            "<tr><td>".$lang['newadmin']."</td><td><select id=\"newadmin\" name=\"newadmin\">".
            "<option value=\"-1\" >".$lang['change_admin']."</option>\n";
if(ADMIN){
    $q = db_query('SELECT id, fullname FROM '.PRE.'users WHERE tech!=\'t\' ORDER BY fullname');
    $users = db_fetch_all($q);
}else{
    /* Select one user to be the adminnistrator of the new category tech manager
     * can choose only between users that are already supervisors of a category
    */
    $q = db_query('SELECT id, fullname FROM '.PRE.'users WHERE catmana=\'t\' ORDER BY fullname');
    $users = db_fetch_all($q); 
}

foreach ($users as $user){
   $content .= "<option value=\"".$user['id']."\" ";
   $content .= ">".$user['fullname']."</option>\n";
}


$content .= "</tr></table>".
            "<p><input type=\"submit\" value=\"".$lang['edit']."\" /></p>\n".
            "</form>";

new_box($lang['category_info'], $content );
?>
