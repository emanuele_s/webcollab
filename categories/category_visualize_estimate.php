<?php
/*
  $Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Add a category to the database
 */
include_once(BASE.'users/user_common.php' );
require_once(BASE.'includes/token.php' );
 
//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/time.php' );

//deny guest users
if(GUEST ) {
 warning($lang['access_denied'], $lang['not_owner'] );
}

$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

validate_token($token, 'estimate_show');

if(!isset($_POST['tasks'])){
    warning('Error', $lang['no_categories_selected']);
}

$tasks = $_POST['tasks'];

$query = 'SELECT tasks.name as name, fullname, owner, category, contact, datediff(date_format(finished_time, \'%Y-%m-%d\'), date_format(created, \'%Y-%m-%d\')) AS time, datediff(date_format(deadline, \'%Y-%m-%d\'), date_format(created, \'%Y-%m-%d\')) AS expected '.
         'FROM tasks '.
         'INNER JOIN users '.
         'ON tasks.creator=users.id '.
         'WHERE tasks.id='.$tasks[0];

for($i = 1; $i<count($tasks); $i++){
    $query .= ' OR tasks.id='.$tasks[$i];
}
$query .= ' ORDER BY tasks.name';
$q = db_query($query);
$tasks = db_fetch_all($q);

$content = '<div align="center"><table class="sortable" id="sort" cellspacing="0" cellpadding="0">'.
           '<tr><th>'.$lang['task_name'].'</th><th>'.$lang['creator_name'].'</th><th>'.$lang['owner_name'].'</th><th>'.$lang['customer_name'].'</th><th class="unsortable">'.$lang['value_days'].'</th><th class="unsortable">'.$lang['expected_days'].'</th><th>'.$lang['categoryTable1'].'</th>';

foreach($tasks as $task){
    $q = db_prepare('SELECT fullname FROM '.PRE.'users WHERE id=?');
    db_execute($q, array($task['owner']));
    $user = db_fetch_array($q); 
    $q1 = db_prepare('SELECT name FROM '.PRE.'categories WHERE id=?');
    db_execute($q1, array($task['category']));
    $category = db_fetch_array($q1); 
    $content .= '<tr><td>'.$task['name'].'</td><td>'.$task['fullname'].'</td><td>'.$user['fullname'].'</td><td>'.($task['contact']==null ? 'no' : $task['contact']).'</td><td>'.$task['time'].'</td><td>'.$task['expected'].'</td><td>'.$category['name'].'</td></tr>';
}

$content .= '</table></div>';

new_box($lang['contributing_tasks'], $content);

?>
