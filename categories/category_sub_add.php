<?php

/*
  $Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Add a category to the database
 */
include_once(BASE.'users/user_common.php' );
require_once(BASE.'includes/token.php' );

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/time.php' );

//deny guest users
if(GUEST ) {
 warning($lang['access_denied'], $lang['not_owner'] );
}

$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

validate_token($token, 'category_add');

if(empty($_POST['catname'])){
    warning('Error', $lang['category_error_name']);
}
$catname = $_POST['catname'];

if(empty($_POST['admin'])){
    warning('Error', $lang['category_error_admin']);
}
$idowner = $_POST['admin'];

$q = db_query('SELECT name FROM '.BASE.'categories');
$catnames = db_fetch_all($q);

foreach ($catnames as $name){
    if(strcasecmp($name['name'], $catname) == 0){
        header("Location: ".BASE_URL."categories.php?x=".X."&action=add&error=e1");
        die;
    }
}

$q = db_prepare('INSERT INTO '.BASE.' categories(name, userid) VALUES (?, ?)');
db_execute($q, array($catname, $idowner));
$q = db_prepare('UPDATE '.PRE.'users SET catmana=? WHERE id=?');
db_execute($q, array('t', $idowner));

if(isset($_POST['redirect'])){
    if(strcasecmp($_POST['redirect'], 'add') == 0){
            header("Location: ".BASE_URL."tasks.php?x=".X."&action=add&parentid=".$_POST['parentid']);
            die;
        
    }
    if(strcasecmp($_POST['redirect'], 'edit') == 0){
            header("Location: ".BASE_URL."tasks.php?x=".X."&action=edit&taskid=".$_POST['taskid']);
            die; 
    }
}

header("Location: ".BASE_URL."main.php?x=".X);
die;
?>
