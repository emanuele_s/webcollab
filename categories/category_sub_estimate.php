<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Show estimates menu
 */
function mean_value($numbers){
    $tot_elements = count($numbers);
    $tot = array_sum($numbers);
    return $tot_elements == 0 ? 0 : (float) $tot/count($numbers);
}

function variance_value($numbers, $mean)
{
  $variance = 0.0;
  $array_sum = array_sum($numbers); 
  if(count($numbers)==0){
      return 0.0;
  }
  for ($i = 0; $i < count($numbers); $i++)
  {
    $variance = $variance + ($numbers[$i] - $mean) * ($numbers[$i] - $mean);
  }
  
  $variance = $variance / count($numbers);
  return $variance;
}

function quartile($numbers, $value){
    $tot = count($numbers);
    switch ($value){
        case 1:
            if($tot%2 == 0){
                $firsthalf = array_slice($numbers, 0, $tot/2);
                if(DEBUG == 'Y'){
                    echo '<br>firstslice pari<br>';
                    print_r($firsthalf);
                }
                return median($firsthalf);
            }else{
                $tot = (int) $tot/2;
                if(DEBUG == 'Y'){
                    echo '<br>firstslice dispari<br>';
                    print_r(array_slice($numbers, 0, $tot));
                }
                return (median(array_slice($numbers, 0, $tot)) + median(array_slice($numbers, 0, $tot+1)))/2;
            }
            break;
        case 3:
            if($tot%2 == 0){
                $secondhalf = array_slice($numbers, $tot/2, $tot/2);
                if(DEBUG == 'Y'){
                    echo '<br>secondslice pari<br>';
                    print_r($secondhalf);
                }
                return median($secondhalf);
            }else{
                $tot = (int) $tot/2;
                if(DEBUG=='Y'){
                    echo '<br>secondslice dispari<br>';
                    print_r(array_slice($numbers, $tot/2+1, $tot/2+2));
                    print_r(array_slice($numbers, $tot/2+2, $tot/2+1));
                }
                return (median(array_slice($numbers, $tot/2+1, $tot/2+2)) + median(array_slice($numbers, $tot/2+2, $tot/2+1)))/2;
            }            
            break;
        default:
            return null;
    }
}

function median($values){
    $tot = count($values);
    if($tot%2 == 0){
        return ($values[$tot/2-1]+$values[$tot/2])/2;
    }else{
        return $values[$tot/2];
    }
}

include_once(BASE.'users/user_common.php' );
require_once(BASE.'includes/token.php' );
include(BASE."pChart2.1.4/class/pDraw.class.php");
include(BASE."pChart2.1.4/class/pImage.class.php");
include(BASE."pChart2.1.4/class/pData.class.php");
include(BASE."pChart2.1.4/class/pStock.class.php");

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/time.php' );

//deny guest users
if(GUEST ) {
 warning($lang['access_denied'], $lang['not_owner'] );
}

$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

if(!empty ($token)){
    validate_token($token, 'category_estimate');
}

generate_token('estimate_show' );

if(!isset($_POST['cate'])){
    header("Location: ".BASE_URL."categories.php?x=".X."&action=estimate&error=no_categories_selected");
}

$categories = $_POST['cate'];
$count = 0;
$totdays = 0;

$query = 'SELECT id, name, datediff(date_format(finished_time, \'%Y-%m-%d\'), date_format(created, \'%Y-%m-%d\')) AS time FROM '.PRE.'tasks WHERE status=\'done\' AND (category='.$categories[0];
for($i = 1; $i<count($categories); $i++){
    $query .= ' OR category='.$categories[$i];
}

$query .= ')';

$q = db_query($query);
$tasks = db_fetch_all($q);

$query = 'SELECT name FROM '.PRE.'categories WHERE id='.$categories[0];
for($i = 1; $i<count($categories); $i++){
    $query .= ' OR id='.$categories[$i];
}

$catnames = array();
$q = db_query($query);
$catna = db_fetch_all($q);

foreach($catna as $c){
    array_push($catnames, $c['name']);
}

$times = array();
$names = array();

foreach($tasks as $task){
    array_push($times, $task['time']);
    array_push($names, $task['name']);
}

if(count($times) == 0){
    header("Location: ".BASE_URL."categories.php?x=".X."&action=estimate&error=no_completed_task");
}

$mean = mean_value($times);
$variance = variance_value($times, $mean);

$close = array();
$open = array();
$min = array();
$max = array();
$median = array();

for($i=0; $i<count($catnames); $i++){
    $q = db_prepare('SELECT id FROM '.PRE.'categories WHERE name=?');
    db_execute($q, array($catnames[$i]));
    $catids = db_fetch_all($q);
    $q = db_prepare('SELECT datediff(date_format(finished_time, \'%Y-%m-%d\'), date_format(created, \'%Y-%m-%d\')) AS time FROM '.PRE.'tasks WHERE category=? AND status=\'done\'');
    db_execute($q, array($catids[0]['id']));
    $v = db_fetch_all($q);
    $values = array();
    foreach ($v as $v){
        array_push($values, $v['time']);
    }
    sort($values);
    if(DEBUG == 'Y'){
        echo "<br>".$catnames[$i]."<br>";
        print_r($values);
    }
    if(empty($values)){
        array_push($close, VOID);
        array_push($open, VOID);
        array_push($min, VOID);
        array_push($max, VOID);
        array_push($median, VOID);
        continue;
    }
    if(count($values)<2){
        switch (count($values)){
            case 0:
                array_push($close, VOID);
                array_push($open, VOID);
                array_push($min, VOID);
                array_push($max, VOID);
                array_push($median, VOID);
                break;
            case 1:
                array_push($close, VOID);
                array_push($open, VOID);
                array_push($min, $values[0]);
                array_push($max, $values[0]);
                array_push($median, $values[0]);
                break;
            default:
                error('Count Error', 'Undefined Error');
        }
    }else{
        $val = quartile($values, 1);
        array_push($close, $val);
        $val = quartile($values, 3);
        array_push($open, $val);
        array_push($min, $values[0]);
        array_push($max, $values[count($values)-1]);
        $val = median($values);
        array_push($median, $val);
    }
}

if(DEBUG == 'Y'){
    echo "<br>open<br>";
    print_r($open);
    echo "<br>close<br>";
    print_r($close);
    echo "<br>min<br>";
    print_r($min);
    echo "<br>max<br>";
    print_r($max);
    echo "<br>median<br>";
    print_r($median);
}

$MyData = new pData();  
$MyData->addPoints($open,"Open");
$MyData->addPoints($close,"Close");
$MyData->addPoints($min,"Min");
$MyData->addPoints($max,"Max");
$MyData->addPoints($median,"Median");

$MyData->addPoints($catnames,"Categories");
$MyData->setAbscissa("Categories");
$MyData->setAbscissaName("Categories");

/* Create the pChart object */
$myPicture = new pImage(500,500,$MyData);

/* Turn of AAliasing */
$myPicture->Antialias = FALSE;

/* Draw the border */
$myPicture->drawRectangle(0,0,499,499,array("R"=>0,"G"=>0,"B"=>0));

$myPicture->setFontProperties(array("FontName"=>BASE."pChart2.1.4/fonts/pf_arma_five.ttf","FontSize"=>6));

/* Define the chart area */
$myPicture->setGraphArea(60,30,450,400);

/* Draw the scale */
$scaleSettings = array("GridR"=>200,"GridG"=>200,"GridB"=>200,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE, "LabelRotation"=>45);
$myPicture->drawScale($scaleSettings);

/* Create the pStock object */
$mystockChart = new pStock($myPicture,$MyData);

/* Draw the stock chart */
$stockSettings = array("BoxUpR"=>255,"BoxUpG"=>255,"BoxUpB"=>255,"BoxDownR"=>0,"BoxDownG"=>0,"BoxDownB"=>0,"SerieMedian"=>"Median");
$mystockChart->drawStockChart($stockSettings);


$myPicture->render(BASE."pChart2.1.4/graph.png");

$content = "<div align=\"center\"><img src=\"".BASE."pChart2.1.4/graph.png\"></div>\n";

$content .= "<div align=\"center\"><table><tr><td><p>".sprintf($lang['mean'], $mean)."</p></td></tr>\n".
            "<tr><td>".sprintf($lang['variance'], $variance)."</td></tr>\n";


if(SHOW_TASKS == 'Y'){
    $content .= "<tr><td><form method=\"post\" action=\"categories.php?x=".X."&amp;action=submit_estimate\">\n".  
                "<fieldset><input type=\"hidden\" name=\"action\" value=\"visualize_estimate\" />\n".
                "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />";
    foreach($tasks as $t){
        $content .=  "<input type=\"hidden\" name=\"tasks[]\" value=\"".$t['id']."\" />\n";
    }

    $content .= "</fieldset><p><input type=\"submit\" value=\"".$lang['tasksconc']."\" /></p>\n".
                "</form></td></tr></table></div>";
}
else{
   $content .= "</table></div>";
}
new_box($lang['statistics'], $content);



?>
 