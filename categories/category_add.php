<?php

/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Add categories
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/token.php' );
include_once(BASE.'users/user_common.php' );

//admin an tech manager only
if(!ADMIN && !TECH){
  error('Unauthorised access', 'This function is for admins only.' );
}

//generate_token
generate_token('category_add' );

if(isset($_GET['redirect'])){
    if(strcasecmp($_GET['redirect'], 'add') == 0 && isset($_GET['parentid']) && safe_integer($_GET['parentid'])){
            $content =  "<form method=\"post\" action=\"categories.php\" ".
                    "onsubmit=\"return fieldCheck('full') \">\n".
                    "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_insert\" />\n".
                    "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
                    "<input type=\"hidden\" name=\"parentid\" value=\"".$_GET['parentid']."\" />\n".
                    "<input type=\"hidden\" name=\"redirect\" value=\"add\" />\n".
                    "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
                    "<input type=\"hidden\" id=\"alert_field\" name=\"alert2\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
                    "<table class=\"celldata\">\n";
    }
    elseif(strcasecmp($_GET['redirect'], 'edit') == 0 && $_GET['taskid'] && @safe_integer($_GET['taskid'])){
            $content =  "<form method=\"post\" action=\"categories.php\" ".
                    "onsubmit=\"return fieldCheck('full') \">\n".
                    "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_insert\" />\n".
                    "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
                    "<input type=\"hidden\" name=\"taskid\" value=\"".$_GET['taskid']."\" />\n".
                    "<input type=\"hidden\" name=\"redirect\" value=\"edit\" />\n".
                    "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
                    "<input type=\"hidden\" id=\"alert_field\" name=\"alert2\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
                    "<table class=\"celldata\">\n";
        
    }else{
        $content =  "<form method=\"post\" action=\"categories.php\" ".
                "onsubmit=\"return fieldCheck('full') \">\n".
                "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_insert\" />\n".
                "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
                "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
                "<input type=\"hidden\" id=\"alert_field\" name=\"alert2\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
                "<table class=\"celldata\">\n";
    }
}else{
    $content =  "<form method=\"post\" action=\"categories.php\" ".
            "onsubmit=\"return fieldCheck('full') \">\n".
            "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_insert\" />\n".
            "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
            "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
            "<input type=\"hidden\" id=\"alert_field\" name=\"alert2\" value=\"".$lang['missing_field_javascript']."\" /></fieldset>\n".
            "<table class=\"celldata\">\n";
}

if(isset($_GET['error'])){
    $content .= "<tr><td><p style=\"color:red;\" >".$lang['e1']."</p></td></tr>".
                "<tr><td>".$lang['category_name']."</td><td><input id=\"full\" type=\"text\" name=\"catname\" class=\"size\" />".
                "<script type=\"text/javascript\">document.getElementById('catname').focus();</script></td></tr>\n".
                "<tr><td>".$lang['administrator']."</td><td><select name=\"admin\">";
}else{
    $content .= "<tr><td>".$lang['category_name']."</td><td><input id=\"full\" type=\"text\" name=\"catname\" class=\"size\" />".
                "<script type=\"text/javascript\">document.getElementById('catname').focus();</script></td></tr>\n".
                "<tr><td>".$lang['administrator']."</td><td><select name=\"admin\">";
}
if(ADMIN){
    //Select one user to be the adminnistrator of the new category
    $q = db_query('SELECT id, fullname FROM '.PRE.'users where tech=\'f\' ORDER BY fullname');
    $users = db_fetch_all($q);
}else{
    /* Select one user to be the adminnistrator of the new category tech manager
     * can choose only between users that are already supervisors of a category
   */
    $q = db_query('SELECT id, fullname FROM '.PRE.'users WHERE catmana=\'t\' ORDER BY fullname');
    $users = db_fetch_all($q);    
}

if(empty ($users)){
    warning('No Users', 'There are no users in the system that can be elected for category manager role ask. Please contact the administrator.');
}

foreach($users as $user) {
   $content .= "<option value=\"".$user['id']."\" ";
   $content .= ">".$user['fullname']."</option>\n";
}
            
$content .= "</tr></table>".
            "<p><input type=\"submit\" value=\"".$lang['add']."\" /></p>\n".
            "</form>";

new_box($lang['category_info'], $content );

?>
