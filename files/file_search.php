<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Search file 
 */

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
require_once(BASE.'includes/admin_config.php' );
require_once(BASE.'includes/token.php' );
require_once(BASE.'includes/usergroup_security.php' );

//deny guest users
if(GUEST ){
 warning($lang['access_denied'], $lang['not_owner'] );
}

//generate_token
generate_token('file_search' );

$content = "<form method=\"post\" action=\"files.php\" onsubmit=\"return fieldCheck('key');\">\n".
           "<input type=\"hidden\" id=\"alert_field\" name=\"alert1\" value=\"".$lang['missing_field_javascript']."\" />\n".
           "<fieldset><input type=\"hidden\" name=\"action\" value=\"submit_search\" />\n".
           "<input type=\"hidden\" name=\"x\" value=\"".X."\" />\n".
           "<input type=\"hidden\" name=\"token\" value=\"".TOKEN."\" />".
           "<table class=\"celldata\">\n".
           "<tr><td>".$lang['select_search']."</td></tr>\n
                <tr><td><label for=\"desc\">".$lang['desc']."</label></td><td><input type=\"checkbox\" id=\"desc\" name=\"searchvar[]\" value=\"2\" checked=\"checked\"></td></tr>\n
                <tr><td><label for=\"deleted\">".$lang['search_del']."</label></td><td><input type=\"checkbox\" id=\"deleted\" name=\"searchvar[]\" value=\"3\"></td></tr>\n
                <tr><td><label for=\"protocol\">".$lang['file_protocol']."</label></td><td><input type=\"checkbox\" id=\"protocol\" name=\"searchvar[]\" value=\"4\"></td></tr>
                <tr><td><label for=\"key\">".$lang['keywords']."</label></td><td><input id=\"key\" type=\"text\" name=\"key\"></td></tr></table>\n";

$content .= "<p><input type=\"submit\" value=\"".$lang['file_search']."\" /></p>\n".
            "</form>";

    new_box($lang['file_search'], $content);
?>      
