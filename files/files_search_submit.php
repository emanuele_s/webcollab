<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Search file 
 */

function look_number($set, $number){
    for($i = 0; $i<count($set); $i++){
        if(strcasecmp($set[$i], $number) == 0){
            return true;
        }
    }
    return false;
}
// 2 desc 3 deleted 4 protocol
function create_query($keywords, $filters){
    $q = "SELECT id, filename, description, protocol FROM ".PRE."files WHERE filename LIKE '%$keywords%'";
    if(look_number($filters, 2)){
        $q .= " OR description LIKE '%$keywords%'";
    }
    if(!look_number($filters, 3)){
        $q .= " AND status !='deleted'";
    }
    if(look_number($filters, 4)){
        $q .= " OR protocol LIKE '%$keywords%'";
    }
    
    return $q;
}

include_once(BASE.'users/user_common.php' );
require_once(BASE.'includes/token.php' );

//security check
if(! defined('UID' ) ) {
  die('Direct file access not permitted' );
}

//includes
include_once(BASE.'includes/admin_config.php' );

//deny guest users
if(GUEST ) {
  warning($lang['access_denied'], $lang['not_owner'] );
}

$token = (isset($_POST['token'])) ? (safe_data($_POST['token'])) : null;

validate_token($token, 'file_search');

if(!isset($_POST['key'])){
    warning('Error', 'Internal Error: keywords missing');
}

$keywords = safe_data($_POST['key']);
if(isset($_POST['searchvar'])){
    $search_filters = $_POST['searchvar'];

    $query = create_query($keywords, $search_filters);

}else{
    $query = "SELECT id, filename, description, protocol FROM ".PRE."files WHERE filename LIKE '%$keywords%'";
}   

$q = db_prepare($query);
db_execute($q);
$files = db_fetch_all($q);

$content = "<table>\n";
foreach ($files as $file){
    $content .= "<tr><td>".$lang['files_detail1']."</td><td><a href=\"files.php?x=".X."&amp;action=download&amp;fileid=".$file['id']."\" onclick=\"window.open('files.php?x=".X."&amp;action=download&amp;fileid=".$file['id']."'); return false\">".$file['filename']."</a></td></tr>".
                "<tr><td>".$lang['files_detail2']."</td><td>".$file['description']."</td></tr>".
                "<tr><td>".$lang['files_detail3']."</td><td>".$file['protocol']."</td></tr>".
                "<tr><td><hr></td></tr>";
}

$content .= "</table>";

new_box('Search Result', $content);

?>
