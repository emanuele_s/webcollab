<?php
/*
$Id$

 (c) 2002 - 2014 Emanuele Santangelo <emanuele.santangelo at gmail.com>

  WebCollab
  ---------------------------------------

  This program is free software; you can redistribute it and/or modify it under the
  terms of the GNU General Public License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
  PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with this
  program; if not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.

  Function:
  ---------

  Send daily mail to users
 */
require_once('path.php');
require_once(BASE.'includes/security.php' );
include_once(BASE.'includes/admin_config.php' );
include_once(BASE.'includes/email.php' );
include_once(BASE.'lang/lang_email.php' );

$q = db_query('SELECT id, fullname, email, morningtasks FROM '.PRE.'users');
$users = db_fetch_all($q);

if(count($users) == 0){
    die;
}
// devi inviare i task che hanno status != done 
/*
 * Task Name : 
 * Text      :
 * Deadline  :
 * 
 */
foreach ($users as $u){
    $content = sprintf($lang['greetings_mail'], $u['fullname']);
    $q = db_prepare('SELECT parent, name, text, deadline FROM '.PRE.'tasks WHERE owner=? AND status!=\'done\' AND parent!=0');
    db_execute($q, array($u['id']));
    
    $tasks = db_fetch_all($q);
    if(count($tasks) == 0){
        continue;
    }
    
    if(isset($_GET['morning'])){
        $q = db_prepare('UPDATE '.PRE.'users SET morningtasks=? WHERE id=?');
        db_execute($q, array(count($tasks), $u['id']));
    }else{
        if(count($tasks) <= $u['morningtasks']){
            $q = db_prepare('UPDATE '.PRE.'users SET morningtasks=? WHERE id=?');
            db_execute($q, array(count($tasks), $u['id']));
            continue;
        }else{
            $q = db_prepare('UPDATE '.PRE.'users SET morningtasks=? WHERE id=?');
            db_execute($q, array(count($tasks), $u['id']));
        }
    }
    foreach ($tasks as $t){
        $content .= sprintf($lang['task_dailymail'], $t['name'], $t['text'], $t['deadline']);
    }
    email($u['email'], $lang['subjectmail'], $content);
}
if(isset($_GET['redirect'])){
    header('Location: '.BASE_URL.'main.php?x='.X);
}
die;
?>
